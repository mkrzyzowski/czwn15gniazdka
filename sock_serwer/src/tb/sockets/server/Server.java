package tb.sockets.server;
 
import java.io.*;
import java.net.*;
import tb.sockets.server.TicGUI;
 
 
 
public class Server
{
   
    static boolean Turn = false, game = true;
    public int prePoz = 10, preRec = 10;
    public static int pozycja = 10, received=10;
    static ServerSocket sersock;
    Socket sock;
    BufferedReader keyRead;
    OutputStream ostream;
    PrintWriter pwrite;
    InputStream istream;
    BufferedReader receiveRead;
    public Server() throws Exception
    {
        sersock = new ServerSocket(6666);
        sock = sersock.accept();
        ostream = sock.getOutputStream();
        pwrite = new PrintWriter(ostream, true);
        istream = sock.getInputStream();
        receiveRead = new BufferedReader(new InputStreamReader(istream));
 
        if(sock.isConnected())
        {
            TicGUI.lbConnected.setText("Connected");
   
                while(game == true)
                {
                    pwrite.flush();
                    if(Turn == true)
                    {
                        if(pozycja!= prePoz)
                        {
                            pwrite.write(pozycja);
                            prePoz = pozycja;
                            Turn = false;
                            pwrite.flush();
                            TicGUI.checkWin();
                            if(TicGUI.gameWon==true) TicGUI.Reset();
                        }
                    }
                    if(Turn == false)
                    {
                       
                        received = receiveRead.read();
                        if(received != preRec)
                        {
                            TicGUI.getOpponentsMove(received);
                            preRec = received;
                            Turn = true;
                            TicGUI.lbTurn.setText("Your turn");
                            TicGUI.checkWin();
                            if(TicGUI.gameWon==true) TicGUI.Reset();
 
                        }
                    }
                    if(sock.isClosed()) {game=false;}
                }
                
           
            }
           
       
            {
                sock.close();
                pwrite.close();
                istream.close();
                ostream.close();
                receiveRead.close();
       
    }
}
}