package tb.sockets.client;


import java.io.*;
import java.net.*;

import tb.sockets.client.TicGUI;

public class Client 
{
	
	static boolean Turn = true, game = true;
	public static int pozycja = 10, received=10;
	public int prePoz = 10, preRec = 10;
	static Socket sock;
	BufferedReader keyRead;
	OutputStream ostream;
	PrintWriter pwrite;
	InputStream istream;
	BufferedReader receiveRead;
	public Client() throws Exception
	{
		sock = new Socket("127.0.0.1", 6666);
		ostream = sock.getOutputStream(); 
		pwrite = new PrintWriter(ostream, true);
		istream = sock.getInputStream();
		receiveRead = new BufferedReader(new InputStreamReader(istream));
		while(sock.isConnected())
		{  
			TicGUI.lbConnected.setText("Connected");
			
		
			{
				while(game==true)
				{
					pwrite.flush();
					if(Turn == true)
					{
						if(pozycja!=prePoz)
						{
							pwrite.write(pozycja);
							prePoz = pozycja;
							Turn = false;
							pwrite.flush();
							TicGUI.checkWin();
							if(TicGUI.gameWon==true) TicGUI.Reset();
						}
					}
					if(Turn == false)
					{	
						
						received = istream.read();
						if(received != preRec)
						{
							TicGUI.getOpponentsMove(received);
							Turn=true;
							preRec = received;
							TicGUI.lbTurn.setText("Your turn");
							TicGUI.checkWin();
							if(TicGUI.gameWon==true) TicGUI.Reset();

						}
					}
					
					if(sock.isClosed())
					{
						game=false;
					}
				}
			
			}



		
				sock.close();
				ostream.close();
				pwrite.close();
				istream.close();
				receiveRead.close();
	
	}	
}}