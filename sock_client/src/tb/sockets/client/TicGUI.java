package tb.sockets.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


class TicGUI extends JFrame 
{

static JButton[][] buttons = new JButton[3][3];
static JFrame frame = new JFrame("Client");                   
JOptionPane turn;
static boolean StartedTurn=true;
static boolean gameWon = false;
public static JLabel lbConnected, lbTurn;
private static int[][] choice;

public TicGUI()                                        
{
 super();
 frame.setSize(350, 400);
 frame.setDefaultCloseOperation(EXIT_ON_CLOSE);       
 frame.setVisible(true);
 frame.setResizable(false);
 choice = new int [3][3];


}

static void checkWin()
{
    if (choice[0][2]==choice[1][2]&& choice[1][2]==choice[2][2]&& choice[2][2]==choice[0][2]&& choice[1][2]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[1][2].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[1][2].getText()+ " wins!!!");
   }
    else if (choice[0][1]==choice[1][1]&& choice[1][1]==choice[2][1]&& choice[2][1]==choice[0][1]&& choice[1][1]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[1][1].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[1][1].getText()+ " wins!!!");
   }
    else if (choice[0][0]==choice[1][0]&& choice[1][0]==choice[2][0]&& choice[2][0]==choice[0][0]&& choice[1][0]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[1][0].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[1][0].getText()+ " wins!!!");
   }
    else if (choice[2][0]==choice[2][1]&& choice[2][1]==choice[2][2]&& choice[2][2]==choice[2][0]&& choice[2][1]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[2][1].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[2][1].getText()+ " wins!!!");
   }
    else if (choice[1][0]==choice[1][1]&& choice[1][1]==choice[1][2]&& choice[1][2]==choice[1][0]&& choice[1][1]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[1][1].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[1][1].getText()+ " wins!!!");
   }
    else if (choice[0][0]==choice[0][1]&& choice[0][1]==choice[0][2]&& choice[0][2]==choice[0][0]&& choice[0][1]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[0][1].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[0][1].getText()+ " wins!!!");
   }
    else if (choice[0][0]==choice[1][1]&& choice[1][1]==choice[2][2]&& choice[2][2]==choice[0][0]&& choice[1][1]!=0)
   {
      gameWon = true;
      Client.Turn = false;
      System.out.println(buttons[1][1].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[1][1].getText()+ " wins!!!");
   }
    else if (choice[0][2]==choice[1][1]&& choice[1][1]==choice[2][0]&& choice[2][0]==choice[0][2]&& choice[1][1]!=0)
   {
      
	  gameWon = true;
	  Client.Turn = false;
      System.out.println(buttons[1][1].getText()+ " wins!!!");
      JOptionPane.showMessageDialog(frame, buttons[1][1].getText()+ " wins!!!");
   }
    else if(choice[0][0]!=0 && choice[0][1]!=0 && choice[0][2]!=0 && choice[1][0]!=0 && choice[1][1]!=0 && choice[1][2]!=0 && choice[2][0]!=0 && choice[2][1]!=0 && choice[2][2]!=0)
    {   gameWon = true;
        Client.Turn = false;
      System.out.println("Stalemate");
      JOptionPane.showMessageDialog(frame, "Stalemate");
    }
}


public static void getOpponentsMove(int wybor)
{
	switch(wybor/3)
	{
	case 0:
	{
		switch(wybor%3)
		{
		case 0:  buttons[0][0].setText("X"); buttons[0][0].setEnabled(false); choice[0][0] = 1; break;
		case 1:  buttons[0][1].setText("X"); buttons[0][1].setEnabled(false); choice[0][1] = 1; break;
		case 2:  buttons[0][2].setText("X"); buttons[0][2].setEnabled(false); choice[0][2] = 1; break;
		}
		break;
	}
	case 1:
	{
		switch(wybor%3)
		{
		case 0:  buttons[1][0].setText("X"); buttons[1][0].setEnabled(false); choice[1][0] = 1; break;
		case 1:  buttons[1][1].setText("X"); buttons[1][1].setEnabled(false); choice[1][1] = 1; break;
		case 2:  buttons[1][2].setText("X"); buttons[1][2].setEnabled(false); choice[1][2] = 1; break;
		}
		break;
	}
	case 2:
	{
		switch(wybor%3)
		{
		case 0:  buttons[2][0].setText("X"); buttons[2][0].setEnabled(false); choice[2][0] = 1; break;
		case 1:  buttons[2][1].setText("X"); buttons[2][1].setEnabled(false); choice[2][1] = 1; break;
		case 2:  buttons[2][2].setText("X"); buttons[2][2].setEnabled(false); choice[2][2] = 1; break;
		}
		break;
	}
	}
}


private void initialize()             // inicjalizacja gry
{
	  JPanel mainPanel = new JPanel(new BorderLayout());         
	  JPanel menu = new JPanel(new BorderLayout());
	  mainPanel.setLayout( new FlowLayout( FlowLayout.CENTER) );
	  menu.setLayout(new GridLayout(2,1));
	  JPanel game = new JPanel(new GridLayout(3,3));                     
	  lbConnected = new JLabel("Disonnected", SwingConstants.CENTER);
	  lbTurn = new JLabel("Your turn", SwingConstants.CENTER);
	  menu.add(lbConnected);
	  menu.add(lbTurn);
	  frame.add(mainPanel);                                         

	  mainPanel.setPreferredSize(new Dimension(325,425));
	  menu.setPreferredSize(new Dimension(300,50));                    
	  game.setPreferredSize(new Dimension(300,300));

	  mainPanel.add(menu, BorderLayout.NORTH);                             
	  mainPanel.add(game, BorderLayout.SOUTH);

	


for(int i = 0; i < 3; i++)                      // tworzenie pola gry
 {
  for(int j = 0; j < 3; j++) 
    {

     buttons[i][j] = new JButton();                 
     buttons[i][j].setText("");
     buttons[i][j].setVisible(true);
     choice[i][j]=0;
     game.add(buttons[i][j]); 
     buttons[i][j].addActionListener(new myActionListener());        
    }
 }

}

private class myActionListener implements ActionListener
{      
 public void actionPerformed(ActionEvent a) 
  {
  
   if(gameWon == false)
   {
   if(a.getSource() == buttons[0][0])                // sprawdzanie jaki przycisk naciskamy oraz interakcja na dany przycisk (wypelnienie pola, wypelnienie wartosci w tablicy oraz zmiana napisu)
     {
	   if(Client.Turn==true)
	   {
       buttons[0][0].setText("O");
       buttons[0][0].setEnabled(false);
       choice[0][0]=2;
       lbTurn.setText("Enemy's turn");
       Client.pozycja = 0;
       
	   }
     } 
   else if(a.getSource() == buttons[0][1])
     {
	   if(Client.Turn==true)
	   {
       buttons[0][1].setText("O");
       buttons[0][1].setEnabled(false);
       choice[0][1]=2;
       lbTurn.setText("Enemy's turn");
       Client.pozycja = 1;
      
	   }
     } 
   else if(a.getSource() == buttons[0][2])
   {
	   if(Client.Turn==true)
	   {
    buttons[0][2].setText("O");
    buttons[0][2].setEnabled(false);
    choice[0][2]=2;
    lbTurn.setText("Enemy's turn");
    Client.pozycja = 2;
    
	   }
   }
   else if(a.getSource() == buttons[1][0])
    {
	   if(Client.Turn==true)
	   {
      buttons[1][0].setText("O");  
      buttons[1][0].setEnabled(false);
      choice[1][0]=2;
      lbTurn.setText("Enemy's turn");
      Client.pozycja = 3;
      
	   }
    } 
   else if(a.getSource() == buttons[1][1])
    {
	   if(Client.Turn==true)
	   {
      buttons[1][1].setText("O");
      buttons[1][1].setEnabled(false);
      choice[1][1]=2;
      lbTurn.setText("Enemy's turn");
      Client.pozycja = 4;
      
	   }
    }
   else if(a.getSource() == buttons[1][2])
    {
	   if(Client.Turn==true)
	   {
      buttons[1][2].setText("O");
      buttons[1][2].setEnabled(false);
      choice[1][2]=2;
      lbTurn.setText("Enemy's turn");
      Client.pozycja = 5;
      
	   }
    } 
   else if(a.getSource() == buttons[2][0])
   {
	   if(Client.Turn==true)
	   {
    buttons[2][0].setText("O");
    buttons[2][0].setEnabled(false);
    choice[2][0]=2;
    lbTurn.setText("Enemy's turn");
    Client.pozycja = 6;
    
	   }
   }
   else if(a.getSource() == buttons[2][1])
   {
	   if(Client.Turn==true)
	   {
    buttons[2][1].setText("O");
    buttons[2][1].setEnabled(false);
    choice[2][1]=2;
    lbTurn.setText("Enemy's turn");
    Client.pozycja = 7;
    
	   }
   }
   else if(a.getSource() == buttons[2][2])
    {
	   if(Client.Turn==true)
	   {
     buttons[2][2].setText("O");
     buttons[2][2].setEnabled(false);
     choice[2][2]=2;
     lbTurn.setText("Enemy's turn");
     Client.pozycja = 8;
     
	   }
    } 
  
 
   
   }
 
  }
 }

public static void Reset()
{
   for(int i = 0; i < 3; i++)
    { 
      for(int j = 0; j < 3; j++)
       {
          gameWon = false;
          buttons[i][j].setText(""); 
          buttons[i][j].setEnabled(true);
          choice[i][j]=0;
          if(StartedTurn==true) Client.Turn=false;
          if(StartedTurn==false) Client.Turn=true;
          StartedTurn = Client.Turn;
       }
    }
  }




public static void main(String[] args) 
{
  
	 TicGUI game = new TicGUI();         
	 new Thread() {
	        public void run() {
	            try {
	                Client client = new Client();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    }.start();
	 game.initialize();
 
 }
}